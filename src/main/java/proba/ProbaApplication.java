package proba;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.ProxyAuthenticationStrategy;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class ProbaApplication {
    public static String URL;
    public static String FILENAME;
    public static String PROXY_HOST;
    public static int PROXY_PORT;
    public static String PROXY_USER;
    public static String PROXY_PASSWORD;
    
    public static void main(String[] argv) throws IOException, URISyntaxException {
        parseArguments(argv);
        requestViaApache(argv);
        //requestViaUnirest(argv);
    }
    public static void parseArguments(String[] argv) {
        final List<String> arguments = Arrays.asList(argv);
        log.info("arguments:{}", arguments);
        if (arguments.size()<2) {
            throw new RuntimeException("need 2 or more arguments: url fileName [proxyHost proxyPort [proxyUser proxyPassword]]");
        }
        URL = arguments.get(0);
        FILENAME = arguments.get(1);
        if (arguments.size() >= 4) {
            PROXY_HOST = arguments.get(2);
            PROXY_PORT= Integer.parseInt(arguments.get(3));
        }
        if (arguments.size() >= 6) {
            PROXY_USER = arguments.get(4);
            PROXY_PASSWORD = arguments.get(5);
        }
    }
    private static void requestViaApache(String[] argv) throws IOException, URISyntaxException {
        log.info("requestViaApache");
        final HttpClientBuilder httpClientBuilder = HttpClients.custom()
                .setConnectionManager(new PoolingHttpClientConnectionManager());
            

            final RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectTimeout(1000000)
                    .setSocketTimeout(1000000)
                    .setConnectionRequestTimeout(1000000)
                    .build();
            if (PROXY_HOST !=null) {
                final HttpHost proxy = new HttpHost(PROXY_HOST, PROXY_PORT);
                httpClientBuilder.setProxy(proxy);
                if (PROXY_USER != null) {
                    final AuthScope proxyAuthScope = new AuthScope(PROXY_HOST, PROXY_PORT);
                    final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
                    credentialsProvider.setCredentials(proxyAuthScope, new UsernamePasswordCredentials(PROXY_USER, PROXY_PASSWORD));
                    httpClientBuilder
                        .setProxyAuthenticationStrategy(new ProxyAuthenticationStrategy())
                        .setDefaultCredentialsProvider(credentialsProvider);
                }
            }
            
            final String requestEntity = IOUtils.toString(ProbaApplication.class.getClassLoader().getResourceAsStream(FILENAME), "UTF-8");
            log.info("requestEntity.length:{}", requestEntity.length());    
        
            final CloseableHttpClient httpClient = httpClientBuilder
                    .setDefaultRequestConfig(requestConfig)
                    .build();

            final HttpPost post = new HttpPost(URL);
            post.setEntity(new StringEntity(requestEntity, "UTF-8"));

        try (CloseableHttpResponse response = httpClient.execute(post)) {
            log.info(response.getStatusLine().toString());
            String body = IOUtils.toString(response.getEntity().getContent(), Charsets.toCharset("UTF-8"));
            log.info(body);
        }
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
/*
    private static void requestViaUnirest(String[] argv) throws IOException, URISyntaxException {
        log.info("requestViaUnirest");
        final List<String> arguments = Arrays.asList(argv);
        log.info("arguments:{}", arguments);

        if (arguments.isEmpty()) {
            throw new RuntimeException("need 1 or more argument. fileName [proxyHost proxyPort [proxyUser proxyPassword]]");
        }

        final HttpClientBuilder httpClientBuilder = HttpClients.custom()
                .setConnectionManager(new PoolingHttpClientConnectionManager());

        if (arguments.size() >= 3) {
            final String proxyHostname = arguments.get(1);
            final int proxyPort = Integer.parseInt(arguments.get(2));

            if (arguments.size() >= 5) {
                final String username = arguments.get(3);
                final String password = arguments.get(4);

                Unirest.config().proxy(proxyHostname, proxyPort, username, password);
            }
            else {
                Unirest.config().proxy(proxyHostname, proxyPort);
            }
        }

        final String fileName = arguments.get(0);

        final String requestEntity = IOUtils.toString(ProbaApplication.class.getClassLoader().getResourceAsStream(fileName + ".txt"), "UTF-8");

        log.info("requestEntity.length:{}", requestEntity.length());

        try {
            final HttpResponse<String> response = Unirest.post("http://192.168.1.102:8085/sign")
                    .body(requestEntity)
                    .charset(Charsets.toCharset("UTF-8"))
                    .queryString("signatureAlgorithm", "GOST_2001")
                    .queryString("digestAlgorithm", "GOST_2001")
                    .queryString("actor", UUID.randomUUID().toString())
                    .asString();

            log.info(response.getStatusText());
            final String body = response.getBody();
            log.info(body);
        }
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    */
}
